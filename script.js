/**
 * Very simple implementation of the Markdown task.
 *
 * I did not use any frameworks due to a lack of time I had to finish the task.
 * (I've spent way too much with Train task)
 * I know this is not what you expected from this task, but it's what I
 * managed to do with the time I had left.
 */
$( document ).ready(function() {
    $(".dropdown-item").click(function (event) {
        var $this     = $(this),
            text      = $this.text(),
            converter = new showdown.Converter(),
            html      = converter.makeHtml(text);

        $("#preview-header").text(text);
        $("#preview").html(html);
    });
});